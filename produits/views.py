from http.client import HTTPResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import Http404
from produits.config import baseUrl
from produits.models import InfoProduct, Transaction, User, UserManager
from rest_framework.permissions import IsAuthenticated
from produits.serializers import InfoProductSerializer, TransactionSerializer, UserSerializer
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.auth import get_user_model
class InfoProductList(APIView):
    
    permission_classes = (IsAuthenticated,)
    def get(self, request, format=None):
        products = InfoProduct.objects.all()
        serializer = InfoProductSerializer(products, many=True)
        return Response(serializer.data)
class InfoProductDetail(APIView):
    def get_object(self, tig_id):
        try:
            return InfoProduct.objects.get(tig_id=tig_id)
        except InfoProduct.DoesNotExist:
            raise Http404
    def get(self, request, tig_id, format=None):
        product = self.get_object(tig_id=tig_id)
        serializer = InfoProductSerializer(product)
        return Response(serializer.data)

class PutProductOnSale(APIView):
    def put(self, request, id, newprice):
        try:
            newprice_float = float(newprice)
            product = InfoProduct.objects.get(id=id)
            product.sale = True
            product.discount = newprice_float
            product.save()

            serializer = InfoProductSerializer(product)
            return Response(serializer.data) 
        except (InfoProduct.DoesNotExist, ValueError):
            return Response({'message': 'Product not found or invalid newprice'}, status=Http404)

class RemoveProductFromSale(APIView):
    def put(self, request, id):
        try:
            product = InfoProduct.objects.get(id=id)
            product.sale = False
            product.discount = 0 
            product.save()

            # Sérialisez l'objet InfoProduct avant de le renvoyer
            serializer = InfoProductSerializer(product)
            return Response(serializer.data)
        except InfoProduct.DoesNotExist:
            return Response({'message': 'Product not found'}, status=Http404)

class IncrementStock(APIView):
    def put(self, request, id, number):
        try:
            number = int(number)
            if number < 0:
                return Response({'message': 'Number must be a positive integer'}, status=400)

            product = InfoProduct.objects.get(id=id)
            product.quantityInStock += number
            product.save()

            serializer = InfoProductSerializer(product)
            return Response({'message': 'Stock incremented successfully', 'product': serializer.data})
        except (InfoProduct.DoesNotExist, ValueError):
            return Response({'message': 'Product not found or invalid number'}, status=404)

class DecrementStock(APIView):
    def put(self, request, id, number):
        try:
            number = int(number)
            if number < 0:
                return Response({'message': 'Number must be a positive integer'}, status=400)

            product = InfoProduct.objects.get(id=id)
            if product.quantityInStock < number:
                return Response({'message': 'Insufficient stock for decrement operation'}, status=400)

            product.quantityInStock -= number
            product.save()

            serializer = InfoProductSerializer(product)
            return Response({'message': 'Stock decremented successfully', 'product': serializer.data})
        except (InfoProduct.DoesNotExist, ValueError):
            return Response({'message': 'Product not found or invalid number'}, status=404)
        

class CreateUser(APIView):
    def post(self, request, format=None):
        User = get_user_model()

        if request.data.get('role') == 'admin':
            user = User.objects.create_superuser(
                username=request.data['username'],
                email=request.data['email'],
                password=request.data['password']
            )
            user.role = 'admin'
            user.save()
        else:
            user = User.objects.create_user(
                username=request.data['username'],
                email=request.data['email'],
                password=request.data['password']
            )
            user.role = 'user'
            user.save()
        
        return Response({'message':'Vous êtes inscrit'})
class GetUsersList(APIView):
    def get(self, request, format=None):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)
    
class GetTransactionList(APIView):
    def get(self, request, format=None):
        transactions = Transaction.objects.all()
        serializer = TransactionSerializer(transactions, many=True)
        return Response(serializer.data)
    

class UpdateStock(APIView):
    def post(self, request, format=None):
        product_id = request.data.get('id')
        modified_stock = request.data.get('modifiedStock')
        achat_prix = request.data.get('achat_prix')

        try:
            product = InfoProduct.objects.get(id=product_id)
            transaction = Transaction(
                product_id=product_id,
                product_stock=modified_stock,
                product_price=achat_prix
            )

            transaction.save()

            product.quantityInStock += modified_stock
            product.save()

            return Response({'message': 'Stock mis à jour avec succès.'})
        except InfoProduct.DoesNotExist:
            return Response({'error': 'Produit non trouvé.'})
        

class Login(APIView):
    def post(self, request, format=None):
        username = request.data.get('username')
        password = request.data.get('password')

        try:
            user = User.objects.get(username=username)

            if user is not None and check_password(password, user.password):   

                return Response({'message':'Vous êtes connecté avec succès'})
            
            return Response({'message':'Mot de passe incorrect'})
                
        except User.DoesNotExist:
            return Response({'error': 'User n existe pas'})



