from rest_framework.serializers import ModelSerializer
from produits.models import InfoProduct, Transaction, User

class InfoProductSerializer(ModelSerializer):
    class Meta:
        model = InfoProduct
        fields = '__all__'


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

class TransactionSerializer(ModelSerializer):
    class Meta:
        model = Transaction
        fields = '__all__'