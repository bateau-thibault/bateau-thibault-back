from django.core.management.base import BaseCommand
from produits.models import User
import time

class Command(BaseCommand):
    users = [
        {
            "username" : "toto",
            "email": "john@example.com",
            "password": "hashed_password_1",
            "role": "admin"
        },
        {
            "username" : "toto2",
            "email": "jane@example.com",
            "password": "hashed_password_2",
            "role": "user"
        },
        {
            "username" : "toto3",
            "email": "bob@example.com",
            "password": "hashed_password_3",
            "role": "user"
        }
    ]

    help = 'Refresh the list of users from TiG server.'

    def handle(self, *args, **options):
        self.stdout.write('[' + time.ctime() + '] Refreshing data...')
        User.objects.all().delete()

        for user_data in self.users:
            user_obj = User(
                username = user_data['username'],
                email=user_data['email'],
                password=user_data['password'],
                role=user_data['role']
            )
            user_obj.save()

            self.stdout.write(self.style.SUCCESS('[' + time.ctime() + '] Successfully added user: %s %s' % (user_obj.first_name, user_obj.last_name)))

        self.stdout.write('[' + time.ctime() + '] Data refresh terminated.')
