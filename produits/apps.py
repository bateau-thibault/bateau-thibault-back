from django.apps import AppConfig


class ProduitConfig(AppConfig):
    name = 'produits'
