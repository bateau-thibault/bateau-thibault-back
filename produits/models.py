from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models

class InfoProduct(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    tig_id = models.IntegerField(default='-1')
    name = models.CharField(max_length=100, blank=True, default='')
    category = models.IntegerField(default='-1')
    price = models.FloatField(default='0')
    unit = models.CharField(max_length=20, blank=True, default='')
    availability = models.BooleanField(default=True)
    sale = models.BooleanField(default=False)
    discount = models.FloatField(default='0')
    comments = models.CharField(max_length=100, blank=True, default='')
    owner = models.CharField(max_length=20, blank=True, default='tig_orig')
    quantityInStock = models.IntegerField(default='30')
    quantity_sold = models.IntegerField(default='20')

    class Meta:
        ordering = ('name',)



class UserManager(BaseUserManager):
    def create_user(self, username, email, password=None):
        if not email:
            raise ValueError("L'adresse e-mail est requise.")
        user = self.model(username=username, email=self.normalize_email(email))
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password=None):
        user = self.create_user(username, email, password)
        user.is_admin = True
        user.is_staff = True
        user.save(using=self._db)
        return user
class User(AbstractUser):


    ROLES = [
        ('admin', 'Admin'),
        ('user', 'User'),
    ]

    role = models.CharField(max_length=5, choices=ROLES, default='user')
    objects = UserManager()


class Transaction(models.Model):

    product_id = models.IntegerField(default=0)
    product_stock = models.IntegerField(default=0)
    product_discount = models.FloatField(default=0)
    product_price = models.FloatField(default=0)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('created_date',)
