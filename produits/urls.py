from django.urls import path
from produits import views
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)


urlpatterns = [
    path('infoproducts/', views.InfoProductList.as_view()),
    path('infoproduct/<int:tig_id>/', views.InfoProductDetail.as_view()),
    path('putonsale/<int:id>/<str:newprice>/', views.PutProductOnSale.as_view()),
    path('removesale/<int:id>/', views.RemoveProductFromSale.as_view()),
    path('incrementStock/<int:id>/<int:number>/', views.IncrementStock.as_view(), name='incrementstock'),
    path('decrementStock/<int:id>/<int:number>/', views.DecrementStock.as_view(), name='decrementstock'),
    path('createuser/', views.CreateUser.as_view(), name='create_user'),
    path('users/', views.GetUsersList.as_view(), name='get_users_list'),
    path('transactions/', views.GetTransactionList.as_view()),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('updatestock/', views.UpdateStock.as_view()),
    path('login/', views.Login.as_view())
]
