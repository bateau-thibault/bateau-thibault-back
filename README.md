## Bateau Thibault Back-end

### Prérequis

1- Clonez le repository : git clone https://gitlab.com/bateau-thibault/bateau-thibault-back.git

2- Installer l'environnement virtuel de Python : `python3 -m venv env` ou `python -m venv env` si vous êtes sur windows

3- Installer Django : `pip3 install django djangorestframework requests`

4- s'assurer qu'on est à jour : `pip install --upgrade pip`

### Problèmes potentiels lors du démarrage du projet

Il est possible que vous rencontriez ces erreurs lors du démarrage : 

- No module named 'rest_framework_simplejwt', pour cela, lancez cette commande : `pip install djangorestframework-simplejwt`

- ModuleNotFoundError: No module named 'pkg_resources', pour cela, lancez cette commande : `pip install setuptools`

- No module named 'corsheaders' : `pip install django-cors-headers`


Une fois tout est fait, vous pouvez lancer le serveur avec : python manage.py runserver



 
